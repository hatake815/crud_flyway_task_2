package kz.aitu.crud.service;

import kz.aitu.crud.repository.CaseERepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CaseEService {
    private final CaseERepository caseERepository;

    public CaseEService(CaseERepository caseERepository) {
        this.caseERepository = caseERepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(caseERepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(caseERepository.findById(id));
    }

    public void deleteByID(long id){
        caseERepository.deleteById(id);
    }

    public void updateByID(long id, String caseHeadingEN){
        caseERepository.updateCaseHeadingEnByID(caseHeadingEN, id);
    }
}

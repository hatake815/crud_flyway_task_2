package kz.aitu.crud.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    private final AuthRepository authorizationNRepository;

    public AuthService(AuthRepository authorizationNRepository) {
        this.authorizationNRepository = authorizationNRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(authorizationNRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(authorizationNRepository.findById(id));
    }

    public void deleteByID(long id){
        authorizationNRepository.deleteById(id);
    }

    public void updateByID(long id, String username){
        authorizationNRepository.updateUsernameByID(username, id);
    }
}

package kz.aitu.crud.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "auth")
public class Auth {
    @Id
    private long id;
    private String username;
    private String email;
    private String password;
    private String role;
    private String forgotpasswordkey;
    private long forgotpasswordkeytimestamp;
    private long companyunitid;
}

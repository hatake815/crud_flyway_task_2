package kz.aitu.crud.repository;

import kz.aitu.crud.model.Nomenclature;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface NomenclatureRepository extends CrudRepository<Nomenclature, Long> {
    @Modifying
    @Transactional
    @Query("update Nomenclature c set c.year =:year where c.id =:ID")
    public void updateYearByID(@Param("year") int year, @Param("ID") long id);
}

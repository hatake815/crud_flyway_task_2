package kz.aitu.crud.repository;

import kz.aitu.crud.model.Case ;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface CaseRepository extends CrudRepository<Case , Long>  {
    @Modifying
    @Transactional
    @Query("update Case c set c.caseheadingen =:caseHeadingEN where c.id =:ID")
    public void updateCaseHeadingEnByID(@Param("caseHeadingEN") String caseHeadingEN, @Param("ID") long id);
}

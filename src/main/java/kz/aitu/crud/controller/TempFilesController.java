package kz.aitu.crud.controller;

import kz.aitu.crud.service.TempFilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TempFilesController {
    private final TempFilesService tempFilesService;

    public TempFilesController(TempFilesService tempFilesService) {
        this.tempFilesService = tempFilesService;
    }

    @GetMapping("/api/tempFiles")
    public ResponseEntity<?> getTempFiles() {
        return ResponseEntity.ok(tempFilesService.getAll());
    }

    @GetMapping("/api/tempFiles/{id}")
    public ResponseEntity<?> findTempFilesByID(@PathVariable long id){
        return ResponseEntity.ok(tempFilesService.findByID(id));
    }

    @DeleteMapping("/api/deleteTempFiles/{id}")
    public void deleteTempFilesByID(@PathVariable long id){
        tempFilesService.deleteByID(id);
    }

    @RequestMapping(value = "/api/updateTempFiles/{id}/{fileBinaryByte}", method = RequestMethod.GET)
    public void updateTempFilesByID(@PathVariable("id") long id, @PathVariable("fileBinaryByte") short fileBinaryByte){
        tempFilesService.updateByID(id, fileBinaryByte);
    }
}

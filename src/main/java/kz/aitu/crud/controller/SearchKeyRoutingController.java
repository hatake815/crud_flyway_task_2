package kz.aitu.crud.controller;

import kz.aitu.crud.service.SearchKeyRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyRoutingController {
    private final SearchKeyRoutingService searchKeyRoutingService;

    public SearchKeyRoutingController(SearchKeyRoutingService searchKeyRoutingService) {
        this.searchKeyRoutingService = searchKeyRoutingService;
    }

    @GetMapping("/api/searchKeyRoutings")
    public ResponseEntity<?> getSearchKeyRoutings() {
        return ResponseEntity.ok(searchKeyRoutingService.getAll());
    }

    @GetMapping("/api/searchKeyRouting/{id}")
    public ResponseEntity<?> findSearchKeyRoutingByID(@PathVariable long id){
        return ResponseEntity.ok(searchKeyRoutingService.findByID(id));
    }

    @DeleteMapping("/api/deleteSearchKeyRouting/{id}")
    public void deleteSearchKeyRoutingByID(@PathVariable long id){
        searchKeyRoutingService.deleteByID(id);
    }

    @RequestMapping(value = "/api/updateSearchKeyRouting/{id}/{tableName}", method = RequestMethod.GET)
    public void updateSearchKeyRoutingByID(@PathVariable("id") long id, @PathVariable("tableName") String tableName){
        searchKeyRoutingService.updateByID(id, tableName);
    }
}

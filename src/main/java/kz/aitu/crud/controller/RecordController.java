package kz.aitu.crud.controller;

import kz.aitu.crud.service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecordController {
    private final RecordService recordService;

    public RecordController(RecordService recordService) {
        this.recordService = recordService;
    }

    @GetMapping("/api/records")
    public ResponseEntity<?> getRecords() {
        return ResponseEntity.ok(recordService.getAll());
    }

    @GetMapping("/api/record/{id}")
    public ResponseEntity<?> findRecordByID(@PathVariable long id){
        return ResponseEntity.ok(recordService.findByID(id));
    }

    @DeleteMapping("/api/deleteRecord/{id}")
    public void deleteRecordByID(@PathVariable long id){
        recordService.deleteByID(id);
    }

    @RequestMapping(value = "/api/updateRecord/{id}/{number}", method = RequestMethod.GET)
    public void updateRecordByID(@PathVariable("id") long id, @PathVariable("number") String number){
        recordService.updateByID(id, number);
    }
}

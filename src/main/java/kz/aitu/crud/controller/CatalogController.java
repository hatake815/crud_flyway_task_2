package kz.aitu.crud.controller;

import kz.aitu.crud.service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogController {
    private final CatalogService catalogService;

    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @GetMapping("/api/catalogs")
    public ResponseEntity<?> getCatalogs() {
        return ResponseEntity.ok(catalogService.getAll());
    }

    @GetMapping("/api/catalog/{id}")
    public ResponseEntity<?> findCatalogByID(@PathVariable long id){
        return ResponseEntity.ok(catalogService.findByID(id));
    }

    @DeleteMapping("/api/deleteCatalog/{id}")
    public void deleteCatalogByID(@PathVariable long id){
        catalogService.deleteByID(id);
    }

    @RequestMapping(value = "/api/updateCatalog/{id}/{nameEN}", method = RequestMethod.GET)
    public void updateCatalogByID(@PathVariable("id") long id, @PathVariable("nameEN") String nameEN){
        catalogService.updateByID(id, nameEN);
    }
}

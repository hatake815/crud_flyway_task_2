package kz.aitu.crud.controller;

import kz.aitu.crud.service.HistoryRequestStatusService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class HistoryRequestStatusController {
    private final HistoryRequestStatusService historyRequestStatusService;

    public HistoryRequestStatusController(HistoryRequestStatusService historyRequestStatusService) {
        this.historyRequestStatusService = historyRequestStatusService;
    }

    @GetMapping("/api/historyRequestStatuses")
    public ResponseEntity<?> getHistoryRequestStatuses() {
        return ResponseEntity.ok(historyRequestStatusService.getAll());
    }

    @GetMapping("/api/historyRequestStatus/{id}")
    public ResponseEntity<?> findHistoryRequestStatusByID(@PathVariable long id){
        return ResponseEntity.ok(historyRequestStatusService.findByID(id));
    }

    @DeleteMapping("/api/deleteHistoryRequestStatus/{id}")
    public void deleteHistoryRequestStatusByID(@PathVariable long id){
        historyRequestStatusService.deleteByID(id);
    }

    @RequestMapping(value = "/api/updateHistoryRequestStatus/{id}/{status}", method = RequestMethod.GET)
    public void updateHistoryRequestStatusByID(@PathVariable("id") long id, @PathVariable("status") String status){
        historyRequestStatusService.updateByID(id, status);
    }
}

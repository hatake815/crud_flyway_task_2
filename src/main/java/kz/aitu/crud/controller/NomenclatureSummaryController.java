package kz.aitu.crud.controller;

import kz.aitu.crud.service.NomenclatureSummaryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureSummaryController {
    private final NomenclatureSummaryService nomenclatureSummaryService;

    public NomenclatureSummaryController(NomenclatureSummaryService nomenclatureSummaryService) {
        this.nomenclatureSummaryService = nomenclatureSummaryService;
    }

    @GetMapping("/api/nomenclatureSummaries")
    public ResponseEntity<?> getNomenclatureSummaries() {
        return ResponseEntity.ok(nomenclatureSummaryService.getAll());
    }

    @GetMapping("/api/nomenclatureSummary/{id}")
    public ResponseEntity<?> findNomenclatureSummaryByID(@PathVariable long id){
        return ResponseEntity.ok(nomenclatureSummaryService.findByID(id));
    }

    @DeleteMapping("/api/deleteNomenclatureSummary/{id}")
    public void deleteNomenclatureSummaryByID(@PathVariable long id){
        nomenclatureSummaryService.deleteByID(id);
    }

    @RequestMapping(value = "/api/updateNomenclatureSummary/{id}/{year}", method = RequestMethod.GET)
    public void updateNomenclatureSummaryByID(@PathVariable("id") long id, @PathVariable("year") int year){
        nomenclatureSummaryService.updateByID(id, year);
    }
}

package kz.aitu.crud.controller;

import kz.aitu.crud.service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestController {
    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @GetMapping("/api/requests")
    public ResponseEntity<?> getRequests() {
        return ResponseEntity.ok(requestService.getAll());
    }

    @GetMapping("/api/request/{id}")
    public ResponseEntity<?> findRequestByID(@PathVariable long id){
        return ResponseEntity.ok(requestService.findByID(id));
    }

    @DeleteMapping("/api/deleteRequest/{id}")
    public void deleteRequestByID(@PathVariable long id){
        requestService.deleteByID(id);
    }

    @RequestMapping(value = "/api/updateRequest/{id}/{requestUserId}", method = RequestMethod.GET)
    public void updateRequestByID(@PathVariable("id") long id, @PathVariable("requestUserId") long requestUserId){
        requestService.updateByID(id, requestUserId);
    }
}

package kz.aitu.crud.controller;

import kz.aitu.crud.service.ShareService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShareController {
    private final ShareService shareService;

    public ShareController(ShareService shareService) {
        this.shareService = shareService;
    }

    @GetMapping("/api/shares")
    public ResponseEntity<?> getShares() {
        return ResponseEntity.ok(shareService.getAll());
    }

    @GetMapping("/api/share/{id}")
    public ResponseEntity<?> findShareByID(@PathVariable long id){
        return ResponseEntity.ok(shareService.findByID(id));
    }

    @DeleteMapping("/api/deleteShare/{id}")
    public void deleteShareByID(@PathVariable long id){
        shareService.deleteByID(id);
    }

    @RequestMapping(value = "/api/updateShare/{id}/{note}", method = RequestMethod.GET)
    public void updateShareByID(@PathVariable("id") long id, @PathVariable("note") String note){
        shareService.updateByID(id, note);
    }
}

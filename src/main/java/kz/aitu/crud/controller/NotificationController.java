package kz.aitu.crud.controller;

import kz.aitu.crud.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NotificationController {
    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/api/notifications")
    public ResponseEntity<?> getNotifications() {
        return ResponseEntity.ok(notificationService.getAll());
    }

    @GetMapping("/api/notification/{id}")
    public ResponseEntity<?> findNotificationByID(@PathVariable long id){
        return ResponseEntity.ok(notificationService.findByID(id));
    }

    @DeleteMapping("/api/deleteNotification/{id}")
    public void deleteNotificationByID(@PathVariable long id){
        notificationService.deleteByID(id);
    }

    @RequestMapping(value = "/api/updateNotification/{id}/{objectType}", method = RequestMethod.GET)
    public void updateNotificationByID(@PathVariable("id") long id, @PathVariable("objectType") String objectType){
        notificationService.updateByID(id, objectType);
    }
}

package kz.aitu.crud.controller;

import kz.aitu.crud.service.DestructionActService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DestructionActController {
    private final DestructionActService destructionActService;

    public DestructionActController(DestructionActService destructionActService) {
        this.destructionActService = destructionActService;
    }

    @GetMapping("/api/destructionActs")
    public ResponseEntity<?> getDestructionActs() {
        return ResponseEntity.ok(destructionActService.getAll());
    }

    @GetMapping("/api/destructionAct/{id}")
    public ResponseEntity<?> findDestructionActByID(@PathVariable long id){
        return ResponseEntity.ok(destructionActService.findByID(id));
    }

    @DeleteMapping("/api/deleteDestructionAct/{id}")
    public void deleteDestructionActByID(@PathVariable long id){
        destructionActService.deleteByID(id);
    }

    @RequestMapping(value = "/api/updateDestructionAct/{id}/{base}", method = RequestMethod.GET)
    public void updateDestructionActByID(@PathVariable("id") long id, @PathVariable("base") String base){
        destructionActService.updateByID(id, base);
    }
}

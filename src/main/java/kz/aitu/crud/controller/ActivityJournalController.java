package kz.aitu.crud.controller;

import kz.aitu.crud.model.ActivityJournal;
import kz.aitu.crud.service.ActivityJournalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActivityJournalController {
    private final ActivityJournalService activityJournalService;

    public ActivityJournalController(ActivityJournalService activityJournalService) {
        this.activityJournalService = activityJournalService;
    }

    @GetMapping("/api/activityJournals")
    public ResponseEntity<?> getActivityJournals(){
        return activityJournalService.getActivityJournals();
    }

    @GetMapping("/api/activityJournal/{id}")
    public ResponseEntity<?> findActivityJournalByID(@PathVariable long id){
        return activityJournalService.findActivityJournalByID(id);
    }

    @DeleteMapping("/api/deleteActivityJournal/{id}")
    public void deleteActivityJournalByID(@PathVariable long id){
        activityJournalService.deleteActivityJournalByID(id);
    }

    @RequestMapping(value = "/api/updateActivityJournal/{id}/{eventType}", method = RequestMethod.GET)
    public void updateActivityJournalByID(@PathVariable("id") long id, @PathVariable("eventType") String eventType){
        activityJournalService.updateActivityJournalByID(id,eventType);
    }

    @PostMapping("/api/ActJour")
    public ResponseEntity<?> createActivityJournal(@RequestBody ActivityJournal activityJournal){
        return ResponseEntity.ok(activityJournalService.create(activityJournal));
    }

}

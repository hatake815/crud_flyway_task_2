package kz.aitu.crud.controller;

import kz.aitu.crud.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyController {
    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("/api/companies")
    public ResponseEntity<?> getCompanies() {
        return ResponseEntity.ok(companyService.getAll());
    }

    @GetMapping("/api/company/{id}")
    public ResponseEntity<?> findCompanyByID(@PathVariable long id){
        return ResponseEntity.ok(companyService.findByID(id));
    }

    @DeleteMapping("/api/deleteCompany/{id}")
    public void deleteCompanyByID(@PathVariable long id){
        companyService.deleteByID(id);
    }

    @RequestMapping(value = "/api/updateCompany/{id}/{nameEN}", method = RequestMethod.GET)
    public void updateCompanyByID(@PathVariable("id") long id, @PathVariable("titleEN") String nameEN){
        companyService.updateByID(id, nameEN);
    }
}

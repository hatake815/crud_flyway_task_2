package kz.aitu.crud.controller;

import kz.aitu.crud.service.CaseIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseIndexController {
    private final CaseIndexService caseIndexService;

    public CaseIndexController(CaseIndexService caseIndexService) {
        this.caseIndexService = caseIndexService;
    }

    @GetMapping("/api/caseIndexes")
    public ResponseEntity<?> getCaseIndexes() {
        return ResponseEntity.ok(caseIndexService.getAll());
    }

    @GetMapping("/api/caseIndex/{id}")
    public ResponseEntity<?> findCaseIndexByID(@PathVariable long id){
        return ResponseEntity.ok(caseIndexService.findByID(id));
    }

    @DeleteMapping("/api/deleteCaseIndex/{id}")
    public void deleteCaseIndexByID(@PathVariable long id){
        caseIndexService.deleteByID(id);
    }

    @RequestMapping(value = "/api/updateCaseIndex/{id}/{titleEN}", method = RequestMethod.GET)
    public void updateCaseByID(@PathVariable("id") long id, @PathVariable("titleEN") String titleEN){
        caseIndexService.updateByID(id, titleEN);
    }
}
